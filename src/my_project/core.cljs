(ns my-project.core
  (:require [my-project.div :as div]))

(defn square [x]
  (* x x))

(defn -main []
  (println (str "5 squared is " (square 5)))
  (println (str "10 divided by 2 is " (div/divide 10 2))))
