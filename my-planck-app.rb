# Documentation: https://github.com/Homebrew/brew/blob/master/share/doc/homebrew/Formula-Cookbook.md
#                http://www.rubydoc.info/github/Homebrew/brew/master/Formula
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!

class MyPlanckApp < Formula
  desc "My Great PlackApp"
  homepage ""
  url "https://gitlab.com/eval/planck-app/repository/archive.tar.gz?ref=v0.1"
  version "0.1"
  sha256 "a40d30609000884d9e85d9e728a14177abc7bc6797a7f1101057c41417ac1ebb"

  # depends_on "cmake" => :build
  depends_on "planck"

  def install
    inreplace "bin/my-project" do |s|
      s.gsub! "-c src", "-c #{lib}"
      #s.gsub! "/usr/share/ansible", pkgshare
      #s.gsub! "/etc/ansible", etc/"ansible"
    end
    lib.install Dir["src/*"]
    bin.install "bin/my-project" => "my-planck-app"
    #bin.install_symlink libexec/"bin"/"my-project"
    # ENV.deparallelize  # if your formula fails when building in parallel

    # Remove unrecognized options if warned by configure
    #system "./configure", "--disable-debug",
    #                      "--disable-dependency-tracking",
    #                      "--disable-silent-rules",
    #                      "--prefix=#{prefix}"
    ## system "cmake", ".", *std_cmake_args
    #system "make", "install" # if this fails, try separate make/make install steps
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! It's enough to just replace
    # "false" with the main program this formula installs, but it'd be nice if you
    # were more thorough. Run the test with `brew test my-planck-app`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "true"
  end
end
