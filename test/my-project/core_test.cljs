(ns my-project.core-test
  (:require [cljs.test :refer-macros [deftest is]]
            [my-project.core]))

(deftest test-square
    (is (= 0 (my-project.core/square 0)))
    (is (= 9 (my-project.core/square 3))))

(cljs.test/run-tests 'my-project.core-test)
